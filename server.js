"use strict"

var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var ParseSDK = require('parse/node');
var twilioClient = require('twilio')('AC9c923a6eb6e0f35a40ab67b3589c8e25', '77757d20523473acbd0261ad4a4d8ff7');
var twilioNumber = '+16502036099';
var path = require('path');
var schedule = require('node-schedule');
var MailTemplates = require('./mail-templates');
var mailTemplates = new MailTemplates;

var omise = require('omise')({
    'publicKey': 'pkey_568fz9hhjp92s1gz70c',
    'secretKey': 'skey_56lg6dmznyuzeolcqx4'
});

var Mailgun = require('mailgun-js');
var MAILGUN_API_KEY = "key-91a827539939799ab171f0873fff9951";
var DOMAIN = "mail.dfastlegal.com"
var mailgun = new Mailgun({ apiKey: MAILGUN_API_KEY, domain: DOMAIN });


var URL = 'https://www.dfastlegal.com/' //This is configure in AWS route53 to target ELB at dfastlegal-elasticbeanstalk-webserver.ap-southeast-1.elasticbeanstalk.com/

var api = new ParseServer({
    databaseURI: 'mongodb://dbadmin:CggqZkttkaAZ8ncf@dfastlegalcluster0-shard-00-00-nxllq.mongodb.net:27017,dfastlegalcluster0-shard-00-01-nxllq.mongodb.net:27017,dfastlegalcluster0-shard-00-02-nxllq.mongodb.net:27017/admin?ssl=true&replicaSet=DFastLegalCluster0-shard-0&authSource=admin',
    //cloud: './cloud/main.js',
    appId: 'sd3f21e65hdf21g6dh5et6h42cv6d5gf4',
    fileKey: '32gf1hjt6y54jf321hj6fg54j9s8regFFG',
    masterKey: 'Argtaer3249sf87gawrhqjkrmdflkhw216SDf',
    serverURL: 'https://server.dfastlegal.com/parse', //This is configure in AWS route53 to target ELB at dfastlegal-elasticbeanstalk-webserver.ap-southeast-1.elasticbeanstalk.com/
    verifyUserEmails: false,
    publicServerURL: 'https://server.dfastlegal.com/parse',
    appName: 'DFastLegal', //app name is required when verify email is true;
    maxUploadSize: '40mb',
    emailAdapter: {
        module: 'parse-server-simple-mailgun-adapter',
        options: {
            // The address that your emails come from
            fromAddress: 'noreply@dfastlegal.com',
            // Your domain from mailgun.com
            domain: 'mail.dfastlegal.com',
            // Your API key from mailgun.com
            apiKey: 'key-91a827539939799ab171f0873fff9951',
        }
    }

});

// Scheduling functions - start

// *    *    *    *    *    *
// ┬    ┬    ┬    ┬    ┬    ┬
// │    │    │    │    │    |
// │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
// │    │    │    │    └───── month (1 - 12)
// │    │    │    └────────── day of month (1 - 31)
// │    │    └─────────────── hour (0 - 23)
// │    └──────────────────── minute (0 - 59)
// └───────────────────────── second (0 - 59, OPTIONAL)

var dailySchedule = schedule.scheduleJob('0 0 1 * * *', function () { // 8.00 am GMT+7 Bangkok

    // var d = new Date();
    // var time = d.getTime();

    // console.log(`This is logged at 3 seconds of every minute: ${d.toLocaleString('en-US')}`);

    //send number of lawyers, active, wait for verification
    lawyersDataUpdate();


});

var weeklySchedule = schedule.scheduleJob({ dayOfWeek: 1, hour: 3, minute: 0, second: 0 }, function () { // every week on monday 10.00 am
    notifyUnsubmittedDocs();

});

// var testWeeklySchedule = schedule.scheduleJob({ dayOfWeek:1, hour:2, minute:45,second:0 }, function () { //Monday 9.45 am

//     var data = {
//         from: 'Legal matching service<service@mail.dfastlegal.com>',
//         to: 'test_mail_list@mail.dfastlegal.com',
//         subject: 'สวัสดีครับคุณ %recipient.firstnameTH% กรุณาอย่าลืมส่งเอกสารเพื่อการยืนยันโปรไฟล์นะครับ',
//         html: mailTemplates.docsRequireMail('test_mail_list@mail.dfastlegal.com', '%recipient_email%', '%recipient.firstnameTH%', '%recipient.firstnameEN%')
//     };

//     mailgun.messages().send(data, function (error, body) {
//         // console.log(body);
//     });


// });


var lawyersDataUpdate = function () {

    var LawyerProfiles = ParseSDK.Object.extend('LawyerProfiles');
    var allLawyerProfile = [];
    var verifiedLawyers = [];
    var toVerifyLawyers = [];
    var query = new ParseSDK.Query(LawyerProfiles);

    var ClientCases = ParseSDK.Object.extend('ClientCases');
    var allClientCase = [];
    var caseIssuedFinding = [];
    var clientCasesQuery = new ParseSDK.Query(ClientCases);

    // console.log('starting query');

    query.find().then((lawyerProfiles) => {

        allLawyerProfile = lawyerProfiles;

        for (let lawyerProfile of allLawyerProfile) {

            if (lawyerProfile.get('verificationRequired') == true) {

                toVerifyLawyers.push(lawyerProfile);


            } if (lawyerProfile.get('isVerified') == true) {

                verifiedLawyers.push(lawyerProfile);

            }

        }

        // console.log(`allLawyers: ${allLawyerProfile.length}, verified lawyers: ${verifiedLawyers.length}, to verify lawyers: ${toVerifyLawyers.length}`);


        return clientCasesQuery.find();


    }).then((clientCases) => {

        allClientCase = clientCases;

        // console.log('query case done');
        // console.log(allClientCase);
        for (let clientCase of allClientCase) {
            // console.log(clientCase.get('isIssued'));
            // console.log(clientCase.get('isDone'));
            if (clientCase.get('isIssued') == true && clientCase.get('isDone') != true) {

                caseIssuedFinding.push(clientCase);

            }

        }

        // console.log(`${allClientCase.length} ${caseIssuedFinding.length}`);
        // console.log(allLawyerProfile);
        // console.log(`allLawyers: ${allLawyerProfile.length}, verified lawyers: ${verifiedLawyers.length}, to verify lawyers: ${toVerifyLawyers.length}`);

        var data = {
            //Specify email data
            from: 'DFastLegal admin<service@mail.dfastlegal.com>',
            //The email to contact
            to: 'khanin@dfastlegal.com, siriphaitun@dfastlegal.com',

            //Subject and text data  
            subject: `Daily lawyers update`,
            html: `
                        <body>
                        <p>All lawyers: ${allLawyerProfile.length}</p>
                        <p>Verified lawyers: ${verifiedLawyers.length}</p>
                        <p>To verify lawyers: ${toVerifyLawyers.length}</p>
                        <p>----------------------------</p>
                        <p>issued and finding lawyer cases: ${caseIssuedFinding.length}</p>
                        </body>
                        `
        }

        //Invokes the method to send emails given the above data with the helper library
        mailgun.messages().send(data, function (err, body) {
            //If there is an error, render the error page
            if (err) {
                // res.render('error', { error: err });
                console.log("got an error: ", err);
            }
            //Else we can greet    and leave
            else {
                //Here "submitted.jade" is the view file for this landing page 
                //We pass the variable "email" from the url parameter in an object rendered by Jade
                // res.render('submitted', { email: req.params.mail });
                console.log('email daily update sent!')
                console.log(body);
            }
        });



    }, (error) => {
        console.log(error.message);
    });

}

var notifyUnsubmittedDocs = function () {

    var data = {
        from: 'Legal matching service<service@mail.dfastlegal.com>',
        to: 'not_yet_sumitted_docs_lawyers_list@mail.dfastlegal.com',
        subject: 'สวัสดีครับคุณ %recipient.firstnameTH% กรุณาอย่าลืมส่งเอกสารเพื่อการยืนยันโปรไฟล์นะครับ',
        html: mailTemplates.docsRequireMail('not_yet_sumitted_docs_lawyers_list@mail.dfastlegal.com', '%recipient_email%', '%recipient.firstnameTH%', '%recipient.firstnameEN%')
    };

    mailgun.messages().send(data, function (error, body) {
        console.log(body);
    });

}

// Scheduling functions - end

var app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/parse', api);


app.get('/unsubscribe-from-mail/:listAddress/:userAddress', function (req, res) {

    var listAddress = req.params.listAddress;
    var userAddress = req.params.userAddress;

    var mailList = mailgun.lists(listAddress);

    mailList.members(userAddress).update({ subscribed: 'false' }, function (err, body) {

        console.log(body);
        res.redirect('https://www.dfastlegal.com/en/unsubscribed');

    });

});
app.get('/unsubscribe/:listAddress/:userAddress', function (req, res) {

    var listAddress = req.params.listAddress;
    var userAddress = req.params.userAddress;

    var mailList = mailgun.lists(listAddress);

    mailList.members(userAddress).update({ subscribed: 'false' }, function (err, body) {

        console.log(body);
        res.end();

    });

});

app.get('/subscribe/:listAddress/:userId', function (req, res) {

    var listAddress = req.params.listAddress;
    var userId = req.params.userId;
    console.log(userId);

    var mailList = mailgun.lists(listAddress);
    console.log(mailList);
    //To query for users, you can simple create a new Parse.Query for Parse.Users:

    var query = new ParseSDK.Query(ParseSDK.User);

    query.get(userId).then((user) => {

        console.log(user);
        var email = user.get('username');
        var firstnameTH = user.get('firstnameTH');
        var lastnameTH = user.get('lastnameTH');
        var firstnameEN = user.get('firstnameEN');
        var lastnameEN = user.get('lastnameEN');

        var newMember = {
            subscribed: true,
            address: email,
            vars: {
                firstnameTH: firstnameTH,
                lastnameTH: lastnameTH,
                firstnameEN: firstnameEN,
                lastnameEN: lastnameEN
            }
        };

        mailList.members().create(newMember, function (err, data) {
            // `data` is the member details 
            console.log(data);
            res.end();
        });

    });

});

app.get('/notify-case/:caseId', function (req, res) {

    console.log('-------------------------Sending email and sms to lawyers in Case--------------------');

    var caseId = req.params.caseId;

    console.log(`caseID: ${caseId}`);


    var ClientCase = ParseSDK.Object.extend('ClientCases');
    var query = new ParseSDK.Query(ClientCase);

    query.include(['selectedLawyers.createdBy']);
    query.include('createdBy');
    query.include('caseType');

    //get client case from param
    query.get(caseId).then((clientCase) => {

        console.log('get case done!');


        sendMailConfirmClient(clientCase);


        // var client = clientCase.get('createdBy');
        // var caseDetail = clientCase.get('caseDetail');
        // console.log(caseDetail);

        // var caseType = clientCase.get('caseType');
        // var amount = caseType.get('contactFee');

        //client info
        var client = parseGetMethod(clientCase, 'createdBy');
        var clientFirstnameTH = parseGetMethod(client, 'firstnameTH');
        var clientLastnameTH = parseGetMethod(client, 'lastnameTH');
        var clientFirstnameEN = parseGetMethod(client, 'firstnameEN');
        var clientLastnameEN = parseGetMethod(client, 'lastnameEN');

        //client's case info
        var caseDetail = parseGetMethod(clientCase, 'caseDetail');
        var lawyerPreferred = parseGetMethod(clientCase, 'lawyerNumber');
        var caseType = parseGetMethod(clientCase, 'caseType');
        var contactFee = parseGetMethod(caseType, 'contactFee');

        var selectedLawyerProfiles = parseGetMethod(clientCase, 'selectedLawyers')

        // creating case-response for each lawyer
        console.log(selectedLawyerProfiles.length);
        // console.log(selectedLawyerProfiles[0]);
        // console.log(parseGetMethod(selectedLawyerProfiles[0], 'createdBy'));

        for (var i = 0; i < selectedLawyerProfiles.length; i++) {

            console.log(selectedLawyerProfiles[i]);

            var lawyer = parseGetMethod(selectedLawyerProfiles[i], 'createdBy');

            console.log(lawyer);

            var LawyerResponse = ParseSDK.Object.extend('LawyerResponse');
            var lawyerResponse = new LawyerResponse();

            lawyerResponse.set('amount', contactFee);
            lawyerResponse.set('client', client);
            lawyerResponse.set('requestedLawyer', lawyer);
            lawyerResponse.set('case', clientCase);
            lawyerResponse.set('isResponse', false);//default value
            lawyerResponse.set('isAccept', false);//default value
            lawyerResponse.set('isPaid', false);//default value

            //save response
            lawyerResponse.save().then((lawyerResponse) => {

                //if success send notification
                var lawyer = parseGetMethod(lawyerResponse, 'requestedLawyer');
                var lawyerEmail = parseGetMethod(lawyer, 'username');
                var lawyerNumber = parseGetMethod(lawyer, 'phoneNumber');
                var formattedLawyerNumber = formatMobileNumber(lawyerNumber);
                var lawyerFirstnameTH = parseGetMethod(lawyer, 'firstnameTH');
                var lawyerFirstnameEN = parseGetMethod(lawyer, 'firstnameEN');

                var responseURL = `https://www.dfastlegal.com/case-response/${lawyerResponse.id}`

                console.log(`sending email to: ${lawyerEmail}`);

                var mailgun = new Mailgun({ apiKey: MAILGUN_API_KEY, domain: DOMAIN });

                var data = {
                    //Specify email data
                    from: 'Legal matching service <service@mail.dfastlegal.com>',
                    //The email to contact
                    to: lawyerEmail,
                    bcc: 'khanin@dfastlegal.com',
                    //Subject and text data  
                    subject: `Potential client needs you, ลูกความกำลังต้องการคุณ`,
                    html: `
                        <body>
                        <h2>Potential client needs you</h2>
                        <h3>Client profile</h3>
                        <p>Name: ${clientFirstnameEN} ${clientLastnameEN}</p>
                        <p>ชื่อ ${clientFirstnameTH} ${clientLastnameTH}</p>
                        <h3>Case Detail</h3>
                        <p>${caseDetail}</p>
                        </br>
                        <p>Please kindly response to this request here<a href="${responseURL}">${responseURL}</a> </p>
                        </body>
                        `
                }

                //Invokes the method to send emails given the above data with the helper library
                mailgun.messages().send(data, function (err, body) {
                    //If there is an error, render the error page
                    if (err) {
                        // res.render('error', { error: err });
                        console.log("got an error: ", err);
                    }
                    //Else we can greet    and leave
                    else {
                        //Here "submitted.jade" is the view file for this landing page 
                        //We pass the variable "email" from the url parameter in an object rendered by Jade
                        // res.render('submitted', { email: req.params.mail });
                        console.log(`email to lawyer sent! (${lawyerEmail})`)
                        console.log(body);
                    }
                });

                //Send an SMS text message
                console.log('sending sms to lawyer');
                console.log(`from: ${twilioNumber}`);
                console.log(`to:${formattedLawyerNumber}`);

                twilioClient.sendMessage({

                    to: formattedLawyerNumber, // Any number Twilio can deliver to
                    from: twilioNumber, // A number you bought from Twilio and can use for outbound communication
                    body: `สวัสดีครับคุณ ${lawyerFirstnameTH} ลูกความได้สนใจขอคำปรึกษาจากท่านกรุณาคลิ๊กที่ลิงค์ด้านล่างเพื่อดำเนินการต่อครับ ${responseURL}, Hi ${lawyerFirstnameEN} potential client needs your help, please click a link to response ${responseURL}`// body of the SMS message

                }, function (err, responseData) { //this function is executed when a response is received from Twilio

                    if (!err) { // "err" is an error received during the request, if any

                        // "responseData" is a JavaScript object containing data received from Twilio.
                        // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
                        // http://www.twilio.com/docs/api/rest/sending-sms#example-1
                        console.log('send notification message to lawyer done!');
                        console.log(responseData.from);
                        console.log(responseData.body);

                    }
                });




            }, (error) => {
                console.log(`error creating response`);
                console.log(error.message);

            });

        }

        console.log('response ended.')
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        console.log('-------------------------- Sending email and sms to lawyers end.---------------------------------');
        res.end();

    });


});

app.get('/send-verification-code/:userId/:caseId', function (req, res) {

    console.log('-------------------Sending sms verification code to client-----------------------');

    var caseId = req.params.caseId;
    var userId = req.params.userId;

    //get code then send sms to user

    var ClientCase = ParseSDK.Object.extend('ClientCases');
    var queryCase = new ParseSDK.Query(ClientCase);

    var queryUser = new ParseSDK.Query(ParseSDK.User);

    console.log('start query case');


    queryCase.get(caseId).then((clientCase) => {

        console.log('Get case done, querying user');

        queryUser.get(userId).then((user) => {

            console.log('get user done');
            var mobileNumber = user.get('phoneNumber');

            var formattedNumber = formatMobileNumber(mobileNumber);
            console.log(formattedNumber);
            var verificationCode = clientCase.get('verificationCode');

            twilioClient.sendMessage({

                to: formattedNumber, // Any number Twilio can deliver to
                from: twilioNumber, // A number you bought from Twilio and can use for outbound communication
                body: `Your verification code for this case is ${verificationCode}`// body of the SMS message

            }, function (err, responseData) { //this function is executed when a response is received from Twilio

                if (!err) { // "err" is an error received during the request, if any

                    // "responseData" is a JavaScript object containing data received from Twilio.
                    // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
                    // http://www.twilio.com/docs/api/rest/sending-sms#example-1
                    console.log('send verification message to client done!');
                    console.log(responseData.from);
                    console.log(responseData.body);



                } else {
                    console.log('send message failed!');

                }

                console.log('----------------Send sms verification end.-----------------------');
                res.end();
            });

        });


    });



});

function formatMobileNumber(mobileNumber) {

    //if first digit is 0
    var formattedNumber = mobileNumber.replace('0', '+66')
    return formattedNumber;

}


function sendMailConfirmClient(clientCase) {

    // var clientEmail = clientCase.get('email');
    // var caseDetail = clientCase.get('caseDetail');
    // var lawyerPreferred = clientCase.get('lawyerNumber');
    console.log('-------------------------------- sending mail to confirm client -----------------------------');

    var client = parseGetMethod(clientCase, 'createdBy');
    var clientEmail = parseGetMethod(client, 'username');
    var caseDetail = parseGetMethod(clientCase, 'caseDetail');
    var lawyerPreferred = parseGetMethod(clientCase, 'preferredLawyer');

    var mailgun = new Mailgun({ apiKey: MAILGUN_API_KEY, domain: DOMAIN });


    var data = {
        //Specify email data
        from: 'Legal matching service<service@mail.dfastlegal.com>',
        //The email to contact
        to: clientEmail,
        bcc: 'khanin@dfastlegal.com',
        //Subject and text data  
        subject: `Your case is issued.`,
        html: `
                        <body>
                        <h2>Your case is issued.</h2>
                        <h3>Case Detail:</h3>
                        <p>${caseDetail}</p>
                        <h3>Number of preferred lawyer:<h3>
                        <p>${lawyerPreferred}</p>
                        </br>
                        <p>Thank you for using our service, please wait and your lawyers will contact you shortly.</p>
                        <p>You can find all your case here: <a href="https://www.dfastlegal.com/client-cases">https://www.dfastlegal.com/client-cases</a></p>
                        </body>
                        `
    }

    //Invokes the method to send emails given the above data with the helper library
    mailgun.messages().send(data, function (err, body) {
        //If there is an error, render the error page
        if (err) {
            // res.render('error', { error: err });
            console.log("got an error: ", err);
        }
        //Else we can greet    and leave
        else {
            //Here "submitted.jade" is the view file for this landing page 
            //We pass the variable "email" from the url parameter in an object rendered by Jade
            // res.render('submitted', { email: req.params.mail });
            console.log('email confirm client sent!')
            console.log(body);
        }
    });

}

function parseGetMethod(object, property) {

    try {
        return object.get(property);
    } catch (error) {
        console.log(error.message);
    }
}

//payment
var paymentRouter = express.Router();

paymentRouter.get('/create/:userId', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");


    var userId = req.params.userId;

    omise.customers.create({
        description: `User ID: ${userId}`
    }, function (error, customer) {
        /* Response. */

        res.json(customer);
    });
})

paymentRouter.get('/retrieveCards/:omiseID', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var omiseID = req.params.omiseID;

    omise.customers.listCards(omiseID, function (error, cardList) {
        /* Response. */
        res.json(cardList);
    });
})

paymentRouter.get('/attach-card/:omiseID/:cardToken', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var omiseID = req.params.omiseID;
    var cardToken = req.params.cardToken;

    omise.customers.update(omiseID, { 'card': cardToken }, function (error, customer) {
        /* Response. */
        res.json(customer);
    });
})

paymentRouter.get('/delete-card/:omiseID/:cardID', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var omiseID = req.params.omiseID;
    var cardID = req.params.cardID;

    omise.customers.destroyCard(
        omiseID,
        cardID,
        function (error, card) {
            /* Response. */
            res.json(card);
        }
    );

})

paymentRouter.get('/pay-case/:omiseID/:cardID/:amount/:responseId', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var omiseID = req.params.omiseID;
    var cardID = req.params.cardID;
    var amountString = req.params.amount;
    var responseId = req.params.responseId;
    amountString = amountString + '00';  //add decimal digits for omise.
    var amount = parseInt(amountString);

    omise.charges.create({
        'description': 'Charge for lawyer response ID: ' + responseId,
        'amount': amount, // 1,000 Baht
        'currency': 'thb',
        'customer': omiseID,
        'capture': true, //this one is buggy, to set it to 'false' just remove it.
        'card': cardID
    }, function (err, response) {
        if (response.paid) {
            //Success
            console.log('success');
            res.json(response);
        } else {
            //Handle failure
            throw response.failure_code;
        }
    });

})

app.use('/payment', paymentRouter);
//!payment

//for submit sitemap
app.use('/sitemap.xml', (req,res)=>{

    // res.sendFile('/sitemap.xml');
    res.header('Content-Type', 'application/xml');
    // res.sendFile(path.resolve(__dirname + '/sitemap.xml'));
    res.sendFile(path.resolve(__dirname + '/sitemap.xml'));
    
})
//!for submit sitemap

// //configure routing to serve angular 2
var renderIndex = (req, res) => {
    res.sendFile(path.resolve(__dirname + '/client', 'index.html'));
    // res.sendFile('/client/index.html');
}

app.use(express.static(path.join(__dirname, 'client'))); //this make a call to client folder and return the app

// app.get('/lawyer-profile/*', renderIndex); //redirect all /lawyer-profile/:id to return index, for entering app from external and refresh the page.

app.get('/*', renderIndex); //redirect any route to return index.html , for entering app from external and refresh the page.this shal be code last after any route above to avoid conflict.


var port = process.env.PORT || 1337;

app.listen(port, function () {
    console.log('parse-server running on port:' + port + '.');
});
